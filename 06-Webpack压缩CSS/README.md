压缩CSS
-------------------

### 插件

压缩CSS需要使用`optimize-css-assets-webpack-plugin`插件。

#### 1、安装依赖

```shell
npm i optimize-css-assets-webpack-plugin
```

为避免版本引发的坑，在这里献出我使用的版本：
```shell
"optimize-css-assets-webpack-plugin": "^6.0.1"
```

#### 2、进行配置

`webpack.config.js`
```javascript
const OptimizeCssAssetsWebpackPlugin = require("optimize-css-assets-webpack-plugin");
...
plugins: [
    new OptimizeCssAssetsWebpackPlugin()
]
```

#### 3、进行验证

运行`webpack`命令进行打包。

没压缩之前打包出来的`main.css`:
```css
#box1 {
    width: 100px;
    height: 100px;
    background-color: pink;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-backface-visibility: visible;
            backface-visibility: visible;
}
#box2 {
    width: 200px;
    height: 200px;
    background-color: skyblue;
}
```

压缩后的`main.css`:
```css
#box1{-webkit-backface-visibility:visible;backface-visibility:visible;background-color:pink;display:-webkit-box;display:-ms-flexbox;display:flex;height:100px;width:100px}#box2{background-color:skyblue;height:200px;width:200px}
```