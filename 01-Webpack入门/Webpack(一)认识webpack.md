认识Webpack
-------------
### 一、准备工作

#### 1.1 环境参数：
NodeJs 10版本以上
Webpack 4.26版本以上

#### 1.2 预备技能：
基本NodeJs知识及npm指令
熟悉ES6语法

### 二、什么是Webpack

- webpack是一种前端构建工具、一个静态模块打包器(module bundler)
- 在webpack看来前端所有资源文件(js/json/css/img/less/…)都会作为模块处理
- 它将根据模块的依赖关系进行静态分析，打包生成对应的静态资源(bundle)

为什么说webpack是一种前端构建工具？
我们创建一个`index.html`文件, 里面写入标题显示在页面上。

`index.html`
```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>认识Webpack</title>
  </head>
  <body>
    <h1 id="title">hello webpack</h1>
  </body>
</html>
```
我们想要给页面添加样式和行为就需要使用css和js，在这里我们期望使用css的预处理器添加样式，使用jquery增加行为，那我们应该如何做呢？
css有许多的预处理器，比如less、scss、stylus等，在这里我们使用less预处理器。新建`index.less`文件并在`index.html引入`
`index.less`:
```less
html,
body {
    margin: 0;
    padding: 0;
    height: 100%;
    background-color: skyblue;
    h1 {
        color: red;
    }
}
```
`index.html`
```html
<link rel="stylesheet" href="index.less">
```

使用jquery添加行为：
首先安装jquery:
```shell
npm init -y
npm i jquery
```
新建index.js文件,并写入：
```javascript
import $ from "jquery";

$("#title").click(() => {
    $("body").css("backgroundColor", "red");
})
```
并在`index.html`中引入
```html
<script src="./index.js"></script>
```
运行页面，显而易见样式和行为文件都是不生效的，因为浏览器无法直接解析less代码，也无法直接解析js的一些语法，需借助工具将less代码解析成css，将不识别的js语法解析成浏览器认识的js代码。

将less解析成css可能需要用到一种小工具，将js进行解析再用到一种小工具，在webpack看来前端所有资源文件(js/json/css/img/less/…)都会作为模块处理，所以页面上其他需要解析的格式也需要不同的工具进行解析成为浏览器识别的内容。那么有没有一种工具把这些小工具集成到一起帮我们完成所有工作呢？`构建工具`便问世了，webpack就是其中的一种，所以说是一种前端构建工具。

为什么说webpack是一个静态模块打包器?
首先指定webpack的入口文件(index.js), 读取index.js中的代码，根据先后顺序依次把相关的依赖模块引进来（当前代码依次是jq、index.less）, 引进来的这些模块就形成了一个Chunk(代码块)，再对着代码块进行各项处理，这些各项处理我们称之为`打包`。经过打包之后生成的资源，我们就可以在浏览器里平稳的运行了，我们打包之后生成的资源叫`bundle`。