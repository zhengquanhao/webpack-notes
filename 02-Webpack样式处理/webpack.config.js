/**
 * webpack.config.js => webpack的配置文件
 * 作用: 指示webpack干哪些活 => 当运行webpack指令的时候，会加载该文件的配置
 * 所有构建工具都是基于NodeJs平台运行的，模块化采用CommonJs
*/

// resolve是用来拼接绝对路径的方法

const { resolve } = require("path");

module.exports = {
    // 入口起点
    entry: "./src/index.js",
    // 输出
    output: {
        // 输出的文件名
        filename: "main.js",
        // 输出的文件路径
        path: resolve(__dirname, "build")
    },
    // loader的配置
    module: {
        // 详细的loader配置
        rules: [
            {
                // 匹配哪些文件，当遇到.css结尾的文件时进行处理
                test: /\.css$/,
                // 使用哪些loader进行处理, 处理顺序从右到左/从上到下
                use: [
                    // 创建style标签，将js中的样式资源插入到html文件的head标签中生效
                    "style-loader",
                    // 将css文件变成CommonJs模块加载到js中，里面内容是样式字符串
                    "css-loader"
                ]
            },
            {
                // 匹配哪些文件，当遇到.less结尾的文件时进行处理
                test: /\.less$/,
                use: [
                    "style-loader",
                    "css-loader",
                    // 将less文件处理为css文件
                    "less-loader"
                ]
            }
        ]
    },
    // 插件的配置
    plugins: [

    ],
    // 模式: 开发模式、生产模式
    mode: "production"
}