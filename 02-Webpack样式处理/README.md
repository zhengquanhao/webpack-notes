使用Webpack处理样式文件
--------------------

### 项目准备

`src`目录下创建入口文件`main.js`并引入样式文件`index.css`

index.css
```css
html, body {
    margin: 0;
    padding: 0;
    background-color:pink;
}
```

`main.js`
```javascript
import "./index.css"
```

### 配置webpack

### 安装解析css的loader

```shell
npm init -y
npm i webpack webpack-cli -D
npm i css-loader style-loader -D
```

### 运行webpack命令
```shell
webpack
```

### 验证打包
打包成功后，在`build`目录下新建`index.html`并引入`main.js`
```html
<script src="./main.js"></script>
```
运行页面发现样式生效，背景呈pink色。

### 处理less

##### 新建less文件并引入

`src/index.less`

```less
html, body {
    margin: 0;
    padding: 0;
    background-color:pink;
    .title {
        color: #ffffff;
    }
}
```
`src/index.js`

```javascript
import "./index.less"
```

##### 安装less解析less语法，安装less-loader处理less文件

```shell
npm i less less-loader -D
```

##### 配置处理less的loader, 需要在rules下面新建一条规则

```javascript
{
    // 匹配哪些文件，当遇到.less结尾的文件时进行处理
    test: /\.less$/,
    use: [
        "style-loader",
        "css-loader",
        // 将less文件处理为css文件
        "less-loader"
    ]
}
```

##### 运行webpack命令及验证结果

验证方式同上

可能less-loader版本过高导致打包报错，这里给出我的依赖版本
```json
 "devDependencies": {
    "css-loader": "^5.2.6",
    "less": "^4.1.1",
    "less-loader": "^5.0.0",
    "style-loader": "^2.0.0",
    "webpack": "^4.46.0",
    "webpack-cli": "^3.3.12"
  }
```