const { resolve } = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/js/index.js",
    output: {
        filename: "js/built.js",
        path: resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "eslint-loader",
                exclude: /node_modules/,
                options: {
                    // 自动修复eslint错误
                    fix: true
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        })
    ],
    mode: "development"
}