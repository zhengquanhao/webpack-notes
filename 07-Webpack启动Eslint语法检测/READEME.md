启动Eslint语法检测
----------------

### 工具

为保证统一团队对同一项目的开发语法规范，需要使用Eslint语法检测，需要使用 `eslint`, `eslint-loader`, `eslint-config-airbnb-base`。

> airbnb 是一套著名的JS语法规范

#### 1、安装依赖

```shell
npm install --save-dev eslint eslint-loader eslint-config-airbnb-base
```

为避免版本引发的坑，在这里献出我使用的版本：
```shell
"eslint": "^7.32.0",
"eslint-config-airbnb-base": "^14.2.1",
"eslint-loader": "^4.0.2"
```

#### 2、进行配置

`webpack.config.js`
```javascript
...
rules: [
    {
        test: /\.js$/,
        loader: "eslint-loader",
        exclude: /node_modules/,
        options: {
            // 自动修复eslint错误
            fix: true
        }
    }
]
```

`package.json`
```javascript
"eslintConfig": {
    "extends": "airbnb-base"
}
```

#### 3、进行验证

运行`webpack`命令进行打包。

未开启fix: true前会显示代码规范错误：
案例：
```shell
1:15  error  A space is required after ','                  comma-spacing
2:1   error  Expected indentation of 2 spaces but found 4   indent
2:17  error  Missing semicolon                              semi
3:2   error  Newline required at end of file but not found  eol-last
```