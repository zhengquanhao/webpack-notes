const { resolve } = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    mode: "development",
    entry: resolve("js/main.js"),
    output: {
        filename: "js/main.js",
        path: resolve("dist")
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    // style-loader作用：创建style标签，将样式嵌入。提取css不需要此插件
                    // "style-loader",
                    // MiniCssExtractPlugin.loader作用：提取js中的css成单独文件
                    MiniCssExtractPlugin.loader,
                    // 将css文件整合到js文件中
                    "css-loader"
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "index.html"
        }),
        new MiniCssExtractPlugin({
            // 对抽离出的css文件进行重命名
            filename: "css/main.css"
        })
    ]
}