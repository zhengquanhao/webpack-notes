const HtmlWebpackPlugin = require("html-webpack-plugin");
const {resolve} = require("path");

module.exports = {
    mode: "development",
    entry: resolve(__dirname, "src/index.js"),
    output: {
        filename: "bundle.js",
        path: resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                use: ["style-loader", "css-loader", "less-loader"]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|jpg|gif)$/,
                loader: "url-loader",
                options: {
                    limit: 8 * 1024,
                    name: "[hash:10].[ext]"
                }
            },
            // 处理html中img资源
            {
                test: /\.html$/,
                loader: "html-loader"
            },
            {
                exclude: /\.(html|css|less|js|png|jpg|gif)$/,
                loader: "file-loader",
                options: {
                    name: "[hash:10].[ext]"
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        })
    ],
    devServer: {
        // 项目构建后的目录
        contentBase: resolve(__dirname, "dist"),
        // 启动gzip压缩
        compress: true,
        port: 3000,
        open: true
    }
}