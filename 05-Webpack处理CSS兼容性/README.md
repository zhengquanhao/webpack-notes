Webpack处理CSS兼容性
-------------------

### 工具

使用`post-css`处理css兼容性, webpack处理css兼容性需要用到`postcss-loader`和`autoprefixer`插件。

#### 1、安装依赖

```shell
npm i postcss-loader autoprefixer -D
```

为避免版本引发的坑，在这里献出我使用的版本：
```shell
"autoprefixer": "^8.6.5",
"postcss-loader": "^3.0.0",
```

#### 2、进行配置

`webpack.config.js`
```javascript
{
    test: /\.css$/,
    use: [
        MiniCssExtractPlugin.loader,
        "css-loader",
        "postcss-loader"
    ]
}
```

`.postcssrc.js`
```javascript
module.exports = {
    "plugins" : {
        "autoprefixer" : {}
    }
};
```

`package.json`
```javascript
"browserslist": [
    "> 1%",
    "last 2 versions",
    "not ie <= 8"
]
```

帮postcss找到`package.json`中的`browerslist`里面的配置，通过配置加载指定的css兼容性样式.

#### 3、进行验证

在css/a.css中添加css3属性
```css
display: flex;
backface-visibility: visible;
```
运行webpack命令，postcss会自动执行autoprefixer插件为属性添加浏览器兼容
打包后的dist/css/main.css
```css
display: -webkit-box;
display: -ms-flexbox;
display: flex;
-webkit-backface-visibility: visible;
        backface-visibility: visible;
```